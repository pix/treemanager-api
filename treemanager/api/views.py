from django.contrib.auth.models import User
from api.models import Tree, Key, TreeChange
from api.serializers import TreeSerializer, KeySerializer, TreeChangeSerializer
from rest_framework import generics
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny, DjangoModelPermissionsOrAnonReadOnly

# Create your views here.


class TreeList(generics.ListAPIView):
    permission_classes = [AllowAny]
    queryset = Tree.objects.all()
    serializer_class = TreeSerializer


class TreeCreate(generics.ListCreateAPIView):
    queryset = Tree.objects.all()
    serializer_class = TreeSerializer


class TreeDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    queryset = Tree.objects.all()
    serializer_class = TreeSerializer


class TreeChangeList(generics.ListAPIView):
    queryset = TreeChange.objects.all()
    serializer_class = TreeChangeSerializer


class TreeChangeCreate(generics.ListCreateAPIView):
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    queryset = TreeChange.objects.all()
    serializer_class = TreeChangeSerializer


class TreeChangeDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    queryset = TreeChange.objects.all()
    serializer_class = TreeChangeSerializer


class KeyList(generics.ListAPIView):
    queryset = Key.objects.all()
    serializer_class = KeySerializer
