from django.contrib.auth.forms import (
    UserCreationForm,
    AuthenticationForm,
    UsernameField,
)
from django.forms import CharField, TextInput, PasswordInput
from django.contrib.auth.password_validation import password_validators_help_text_html
from django.contrib.auth import get_user_model


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ("username", "password1", "password2")


class CustomAuthenticationForm(AuthenticationForm):
    class Meta:
        model = get_user_model()
        fields = ("username", "password")


class RegisterForm(CustomUserCreationForm):
    username = UsernameField(
        widget=TextInput(
            attrs={
                "autofocus": True,
                "placeholder": "Username",
                "class": "form-control"
            }
        )
    )
    password1 = CharField(
        widget=PasswordInput(
            attrs={
                "placeholder": "Password",
                "class": "form-control"
            }
        ),
        label="Password",
    )
    password2 = CharField(
        widget=PasswordInput(
            attrs={
                "placeholder": "Retype Password",
                "class": "form-control"
            }
        ),
        label="Retype Password",
        help_text="For confirmation you need to retype your password",
    )


class LoginForm(CustomAuthenticationForm):
    username = UsernameField(
        widget=TextInput(
            attrs={
                "autofocus": True,
                "placeholder": "Username",
                "class": "form-control"
            }
        )
    )
    password = CharField(
        widget=PasswordInput(
            attrs={
                "placeholder": "Password",
                "class": "form-control"
            }
        )
    )

    error_messages = {
        "invalid_login": "Invalid login credentials",
        "inactive": "User is not activated. Please contact an administrator",
    }
