from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from accounts.models import User


class UserAdmin(BaseUserAdmin):
    list_display = ("username", "is_staff", "is_superuser")
    search_fields = ("username",)
    ordering = ("username",)
    fieldsets = (
        ("Basic Information", {"fields": ("username", "password", "is_active")}),
        (
            "Permissions",
            {
                "fields": (
                    "is_staff",
                    "is_superuser",
                    "groups",
                )
            },
        ),
    )


admin.site.register(User, UserAdmin)
