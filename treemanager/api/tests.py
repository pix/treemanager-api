from django.test import TestCase
from api.models import Tree, TreeChange
from api.serializers import TreeSerializer, TreeChangeSerializer
import json


class TestTreeModel(TestCase):
    def setUp(self):
        change1 = TreeChange.objects.create(
            heigth=1.39, tags={"bepinselt": "ja"}
        )
        change2 = TreeChange.objects.create(
            heigth=1.45, tags={"bepinselt": "ja"}
        )
        tree = Tree.objects.create(
            created_at="2022-04-17",
            name="A2",
            species="Buche",
            geom='{"type": "Point", "coordinates": [4.9932861328125, 47.00694814478153]}',
        )
        tree.changes.add(change1)
        tree.changes.add(change2)

    def test_tree_latlon(self):
        """
        Test the latlon property
        """
        tree = Tree.objects.get(pk=1)
        self.assertEquals(tree.latlon, [i for i in tree.geom])

    def test_tree_str(self):
        """
        Test the latlon property
        """
        tree = Tree.objects.get(pk=2)
        self.assertEquals(
            tree.__str__(),
            "Tree %s at: [%s, %s]"
            % (
                tree.name,
                str(tree.latlon[0])[:7],
                str(tree.latlon[1])[:7],
            ),
        )
